#!/usr/bin/env python3
import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year TEXT, title TEXT, director TEXT, actor TEXT, release_date TEXT, rating DECIMAL(7,2), PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:

        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    # cur = cnx.cursor()
    # cur.execute("INSERT INTO message (greeting) values ('Hello, World!')")
    # cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT greeting FROM message")
    entries = [dict(greeting=row[0]) for row in cur.fetchall()]
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None



@app.route('/add_movie', methods=['POST'])
def add_to_db():
    print("Received request.")
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur0 = cnx.cursor(buffered=True)
    sql = ("SELECT * FROM movies WHERE title='%s'" % (title))
    cur0.execute(sql)
    if(cur0.rowcount > 0):
        return render_template('index.html', message="Movie " + title + " could not be inserted - Movie already exists")

    sql = ("INSERT INTO movies (year, title, director, actor, release_date, rating) VALUES (%s, %s, %s, %s, %s, %s)")
    val = (year, title, director, actor, release_date, rating)

    try:
        cur.execute(sql, val)
        cnx.commit()
        return render_template('index.html', message="Movie " + title + " successfully inserted")
    except Exception as exp:
        return render_template('index.html', message="Movie " + title + " could not be inserted - " + str(exp))
@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Received request.")
    delete_title = request.form['delete_title']

    db, username, password, hostname = get_db_creds()
    
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    delete_title = delete_title.lower()
    cur = cnx.cursor()
    cur0 = cnx.cursor(buffered=True)
    sql = ("SELECT * FROM movies WHERE title='%s'" % (delete_title))
    cur0.execute(sql)
    if(cur0.rowcount == 0):
         return render_template('index.html', message="Movie " + delete_title + " could not be deleted - Movie with " + delete_title + " does not exist")
    sql = ("DELETE FROM movies WHERE LOWER(title) = '%s'" % delete_title)
    try:
        cur.execute(sql)
        cnx.commit()
        return render_template('index.html', message="Movie " + delete_title + " successfully deleted")
    except Exception as exp:
        return render_template('index.html', message="Movie " + delete_title + " could not be deleted- " + str(exp))

@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request.")
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur0 = cnx.cursor(buffered=True)
    sql = ("SELECT * FROM movies WHERE title='%s'" % (title))
    cur0.execute(sql)
    if(cur0.rowcount == 0):
         return render_template('index.html', message="Movie " + title + " could not be updated - Movie with " + title + " does not exist")
    sql = ("UPDATE movies SET year='%s', director='%s', actor='%s', release_date='%s', rating='%s' WHERE title='%s'" % (year, director, actor, release_date, rating, title))
    try:
        cur.execute(sql)
        cnx.commit()
        return render_template('index.html', message="Movie " + title + " successfully updated")
    except Exception as exp:
        return render_template('index.html', message="Movie " + title + " could not be updated- " + str(exp))


@app.route('/search_movie', methods=['GET'])
def search_movie():
    print("Received request.")
    actor = request.args.get('search_actor')

    db, username, password, hostname = get_db_creds()
    
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    actor = actor.lower()
    results = []
    cur = cnx.cursor() 
    sql = ("SELECT * FROM movies WHERE LOWER(actor)='%s'" % (actor))
    cur.execute(sql)

    for row in cur:
        title = str(row[2])
        year = str(row[1])
        actor = str(row[4])
        tup = (title, year, actor)
        results.append(tup)

    cnx.commit()

    if len(results) == 0:
        return render_template('index.html', message ="No movies found for actor '%s" %(actor))
    return render_template('index.html', results = results)

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    print("Received request.")

    db, username, password, hostname = get_db_creds()
    
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    results = []
    cur = cnx.cursor() 
    sql = ("SELECT * FROM movies WHERE rating=(SELECT max(rating) FROM movies)")
    cur.execute(sql)
    for row in cur:
        title = str(row[2])
        year = str(row[1])
        actor = str(row[4])
        director = str(row[3])
        rating = str(row[6])

        tup = (title, year, actor, director, rating)
        results.append(tup)

    cnx.commit()
    if len(results) == 0:
        return render_template('index.html', message ="No movies in database")
    return render_template('index.html', results = results)

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    print("Received request.")

    db, username, password, hostname = get_db_creds()
    
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    results = []
    cur = cnx.cursor() 
    sql = ("SELECT * FROM movies WHERE rating=(SELECT min(rating) FROM movies)")
    cur.execute(sql)
    for row in cur:
        title = str(row[2])
        year = str(row[1])
        actor = str(row[4])
        director = str(row[3])
        rating = str(row[6])

        tup = (title, year, actor, director, rating)
        results.append(tup)

    cnx.commit()
    if len(results) == 0:
        return render_template('index.html', message ="No movies in database")
    return render_template('index.html', results = results)


@app.route("/")
def hello():
    print("Inside hello")
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    entries = query_data()
    print("Entries: %s" % entries)
    return render_template('index.html', entries=entries)

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')